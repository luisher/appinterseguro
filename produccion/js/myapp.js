'use strict';

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

$(document).ready(function () {
  datepicker();
  function datepicker() {
    $('.datepicker').datepicker({
      dateFormat: 'dd/mm/yy',
      showButtonPanel: false,
      changeMonth: false,
      changeYear: false,
      inline: true
    });
    $.datepicker.regional['es'] = {
      closeText: 'Cerrar',
      prevText: '<Ant',
      nextText: 'Sig>',
      currentText: 'Hoy',
      monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
      monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
      dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
      dayNamesShort: ['Dom', 'Lun', 'Mar', 'Mié', 'Juv', 'Vie', 'Sáb'],
      dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sá'],
      weekHeader: 'Sm',
      dateFormat: 'dd/mm/yy',
      firstDay: 1,
      isRTL: false,
      showMonthAfterYear: false,
      yearSuffix: ''
    };
    $.datepicker.setDefaults($.datepicker.regional['es']);
  }

  /*VALIDACIONES*/
  (function ($) {
    jQuery.fn.NumberOnly = function () {
      return this.each(function () {
        $(this).keydown(function (e) {
          var key = e.charCode || e.keyCode || 0;
          return key == 8 || key == 9 || key == 13 || key >= 35 && key <= 40 || key >= 48 && key <= 57 || key >= 97 && key <= 104;
        });
      });
    };
    $('#nropasajeros').NumberOnly();
  })(jQuery);
  $(".js-example-responsive").select2({
    width: 'resolve' // need to override the changed default
  });
});
var PLANES;
var COBERTURA;

var Viaje = function Viaje(destino, fechaPartida, fechaRetorno, nropasajeros) {
  _classCallCheck(this, Viaje);

  this.destino = destino;
  this.fecha_partida = fechaPartida;
  this.fecha_retorno = fechaRetorno;
  this.cantidad_pasajeros = nropasajeros;
};

function llenarCombo() {
  var combo = document.getElementById("select");
  fetch('https://testsoat.interseguro.com.pe/talentfestapi/destinos').then(function (response) {
    return response.json();
  }).then(function (data) {
    var destinos = [];
    data.forEach(function (element) {
      destinos += "<option>" + element + "</option>";
    });
    combo.innerHTML = destinos;
    return data;
  }).catch(function (error) {
    return console.error(error);
  });
}
function getCheckbox(aplica) {
  if (aplica) {
    return '<input type="checkbox" checked/>';
  }
  return '<input type="checkbox" />';
}
function mostrarTabla() {
  document.getElementById("cotizadorViajes").style.display = "block";
}
function getCotizacion() {
  var objViaje = new Viaje();
  var formData = new FormData();
  objViaje.destino = document.getElementById("select").value;
  objViaje.fecha_partida = document.getElementById("fechaPartida").value;
  objViaje.fecha_retorno = document.getElementById("fechaRetorno").value;
  objViaje.cantidad_pasajeros = document.getElementById("nropasajeros").value;

  formData.append('destino', objViaje.destino);
  formData.append('fecha_partida', objViaje.fecha_partida);
  formData.append('fecha_retorno', objViaje.fecha_retorno);
  formData.append('cantidad_pasajeros', objViaje.cantidad_pasajeros);

  console.log(formData);
  fetch('https://testsoat.interseguro.com.pe/talentfestapi/cotizacion', {
    method: 'POST',
    body: formData
  }).then(function (response) {
    return response.json();
  }).catch(function (error) {
    return console.error('Error:', error);
  }).then(function (response) {
    console.log(response);
    PLANES = "";
    for (var i = 0; i < response.length; i++) {
      PLANES += '<div class="planes__table--item">\n                      <h4>\n                        <strong id="titulo-plan">' + response[i].tipo_plan + '</strong>\n                      </h4>\n                      <p id="precio-plan">' + response[i].precio_plan + '</p>\n                  </div>';
    }

    COBERTURA = "";
    var caracteristicas1 = response[0].caracteristicas;
    var caracteristicas2 = response[1].caracteristicas;
    for (var j = 0; j < caracteristicas2.length; j++) {
      var caracteristica1 = caracteristicas1[j];
      var caracteristica2 = caracteristicas2[j];
      COBERTURA += '<tr>\n                          <td>' + caracteristica1.caracteristica + '</td>\n                          <td>\n                              ' + getCheckbox(caracteristica1.aplica) + '\n                          </td>\n                          <td>\n                              ' + getCheckbox(caracteristica2.aplica) + '\n                          </td>\n                        </tr>';
    }

    document.getElementById("planes__table").innerHTML = PLANES;
    document.getElementById("cobertura").innerHTML = COBERTURA;
    mostrarTabla();
  });
}

llenarCombo();

document.querySelector("#cotizar").addEventListener("click", getCotizacion);